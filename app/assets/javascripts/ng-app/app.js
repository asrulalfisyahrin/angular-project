twitterApiApp = angular.module('twitterapi', [
        'ui.router','templates', 'twitterapi.controllers', 'twitterapi.services', 'ngResource',
        'Devise', 'angular-loading-bar', 'ngAnimate', 'validation.match', 'ckeditor'
    ]);

twitterApiApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.latencyThreshold = 500;
  }]);

twitterApiApp.config(function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'home.html',
            controller: 'HomeController'
        })

        .state('list_users', {
            url: '/users',
            templateUrl: 'users/index.html',
            controller: 'UsersController'
        })

        .state('add_user', {
            url: '/users/add',
            templateUrl: 'users/add.html',
            controller: 'UsersController'
        })

        .state('edit_user', {
            url: '/users/:id/edit',
            templateUrl: 'users/edit.html',
            controller: 'UsersController'
        })

        .state('list_articles', {
            url: '/articles',
            templateUrl: 'articles/index.html',
            controller: 'ArticlesController'
        })

        .state('add_article', {
            url: '/articles/add',
            templateUrl: 'articles/add.html',
            controller: 'ArticlesController'
        })

        .state('edit_article', {
            url: '/articles/:id/edit',
            templateUrl: 'articles/edit.html',
            controller: 'ArticlesController'
        });
});