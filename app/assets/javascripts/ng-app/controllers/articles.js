twitterApiApp

.controller('ArticlesController', ['$scope', '$stateParams', '$resource', 'Article', 'Articles', '$location',
	function($scope, $stateParams, $resource, Article, Articles, $location){

		// Editor options.
		$scope.options = {
		    language: 'en',
		    allowedContent: true,
		    entities: false
		}

		// Called when the editor is completely ready.
		$scope.onReady = function () {
		    // ...
		};

		$scope.index = function(){
			Articles.query();
		}

		$scope.new = function(){
			$scope.article = {};
		}

		$scope.create = function(){
			if($scope.articleForm.$valid){
				Articles.create({ article: $scope.article }, function(){
					$location.path('/articles');
				}, function(error){
					console.log(error);
				});
			}
		}

		$scope.edit = function(){
			$scope.article = Article.get({ id: $stateParams.id });
		}

		$scope.update = function(){
			if($scope.articleForm.$valid){
				Article.update($scope.article, function(){
					$location.path('/articles');
				}, function(error){
					console.log(error);
				});
			}
		}

		$scope.destroy = function(article_id){
			if(confirm('Yakin menghapus artikel dengan id '+article_id+" ?")){
				Article.delete({id: article_id}, function(){
					$scope.articles = Articles.query();
					$location.path('/articles');
				});
			}
		}
	}
]);