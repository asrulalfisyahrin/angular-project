var appCtrl = angular.module('twitterapi.controllers', []);

appCtrl.controller('HomeController', function ($scope, $location) {
    $scope.things = ['Angular', 'Rails 4.1', 'Working', 'Together!!'];

    // set to list users
	$scope.listUserPage = function(){
		$location.path('/users');
	}
});