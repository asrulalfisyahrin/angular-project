twitterApiApp

.controller('UsersController', ['$scope', '$resource', '$stateParams', 'UsersFactory', 'UserFactory', '$location', 'flash',
	function($scope, $resource, $stateParams, UsersFactory, UserFactory, $location, flash) {

        $scope.flash = flash;
        
        $scope.new = function(){
            $scope.user = {}
        }

        $scope.edit = function(){
            $scope.user = UserFactory.get({id: $stateParams.id})    
        }
        
    	// get data users
        $scope.list = function(){
            flash.setMessage('Successfully create new user');
            $scope.users = UsersFactory.query();
        }

    	// input data user
    	$scope.create = function(){
    		if ($scope.userForm.$valid){
                UsersFactory.create({user: $scope.user}, function(){
                    flash.setMessage('Successfully create new user');
                    $location.path('/users');
                }, function(error){
                    console.log(error)
                });
            }
    	}

        $scope.update = function(){
            if($scope.userForm.$valid){
                UserFactory.update($scope.user, function(){
                    $location.path('/users');
                }, function(error){
                    console.log(error);
                });
            }
        }

        $scope.delete = function(user_id){
            if(confirm('Yakin hapus user dengan id'+user_id+" ?")){
                UserFactory.delete({id: user_id}, function(){
                    $scope.users = UsersFactory.query();
                    $location.path("/users");
                });
            }
        }

        $scope.cancel = function(){
            $location.path('/users');
        }


	}
]);